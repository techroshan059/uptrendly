<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUtInfluencerInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ut_influencer_info', function (Blueprint $table) {
            $table->increments('id')->primarykey();
            $table->unsignedInteger('user_id');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('avatar');
            $table->unsignedInteger('profession_id');
            $table->string('username');
            $table->string('target_audience');
            $table->string('target_audience_age');
            $table->longText('interested_for');
            $table->longText('not_interested_for');
            $table->float('price_per_content',10,2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ut_influencer_info');
    }
}
